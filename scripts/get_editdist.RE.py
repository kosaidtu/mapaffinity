import sys,json,re

edit_dist = {}
pat = re.compile("NM\:i\:(\d+)") #dict. with edit dists.
for line in sys.stdin:
 i = pat.findall(line)
 if i:
  i = int(i[0])
  edit_dist[i] = edit_dist.setdefault(i,0) + 1


plist = []
for k,v in edit_dist.items():
 plist.append(str(k))
 plist.append(str(v))
print(",".join(plist))
#print(edit_dist.keys())
#print(edit_dist.values())

json.dump(edit_dist,open(sys.argv[1]+".json","w"),sort_keys = True,
        indent = 4, ensure_ascii = False)
