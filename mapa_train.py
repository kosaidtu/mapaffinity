#!/usr/bin/env python
import argparse
import os,sys,re,numpy,glob

#ML packages
import matplotlib.pyplot as plt
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import ShuffleSplit
from sklearn.metrics import matthews_corrcoef
from sklearn import metrics
from scipy import interp



parser = argparse.ArgumentParser(prog="mapaffinity_train",usage="python mapa_train.py -i editdistances.A editdistances.B (editdistances.C etc) -f fileA.txt fileB.txt (...fileC.txt etc) -l A B (C etc) -lt A B -o output_folder",description="Version 1 (2018)\nScreens (ancient) NGS reads against pathogens to determine their presence. This script trains a new model based on two user-defined closely related organisms.",formatter_class=argparse.RawDescriptionHelpFormatter,
     epilog='''
----------------------------------------------
Tutorial
----------------------------------------------
For two organisms, A and B, run mapa_map.py
to get the following files:
- editdistances.A
- editdistances.B

Put the sample id's (e.g. "reads1.fq.gz") in
a single column text file per organism with
one sample per row. For paired end fastq-files,
use the name of the first pair:
  >cat fileA.txt:
   reads1.fq.gz
   reads2.fq
   reads3.fq
   reads4.fastq

Make sure you know the labels for each of the
above files, fileA.txt, fileB.txt, etc.

Run this program like this:
python mapa_train.py
-i editdistances.A editdistances.B
-f fileA.txt fileB.txt
-l A B
-lt A B
-o output_folder

-l can be multiple labels for the plots
and ordered after the -f argument.
-lt must be two of the labels from -l,
where the first one is the target, and
second one is the close organism.
Note that even if -l has multiple labels
the model is still a binary classifier,
but shows a useful probability plot where
each individual organism os colored.

See https://bitbucket.org/kosaidtu/mapaffinity for full documentation.''')
parser.add_argument("-i", metavar="FILE",help="Input SPACE-sep. list of editdistances files. (Required)", nargs="+")
parser.add_argument("-f", metavar="FILE",help="Input SPACE-sep. list of files with sample ids. (Required)", nargs="+")
parser.add_argument("-l", metavar="FILE",help="Input SPACE-sep. list of labels for files in -f argument. Same order as in -f. (Required)", nargs="+")
parser.add_argument("-lt", metavar="FILE",help="Input SPACE-sep. list of Pathogen-label and Close-organism-label. (Required 2 arguments from -l)", nargs=2)
parser.add_argument("-o", metavar="NAME",help="Output folder name. (Required)",nargs=1,required=True)
parser.add_argument("-t", metavar="INT",help="Number of threads. Default=2",type=int,default=2)
parser.add_argument("-q",help="Do NOT print statistics to STDOUT.",action="store_true")
#parser.add_argument("--path",help="Use programs in $PATH instead of included pre-compiled binaries",action="store_true")

args = parser.parse_args()
#args = parser.parse_args('-i naive_bayes/salmonella/fasta/{0}_1.fastq naive_bayes/salmonella/fasta/{0}_2.fastq -o ./naive_bayes/salmonella/{0} -d 3'.format(salm).split()) #test

if not args.q:
    print(args)


### Checking lengths of arguments match
if len(args.i) == len(args.lt) == 2 and (len(args.f) == len(args.l)):
 N = len(args.i)
 myreport = """MODEL PERFORMANCES
------------------
""" #Here we save statistics, performances etc
else:
 sys.exit("Mismatch between number of input files/labels, -i ({0}), -f ({1}), -l ({2}), -lt ({3})".format(len(args.i),len(args.f),len(args.l),len(args.lt)))

#### Preparing output-folder
opath = args.o[0]
if not os.path.exists(opath):
    os.mkdir(opath)
    os.mkdir(opath+"/plots")
#    os.mkdir(opath+"/log")
else:
    sys.exit("Output folder already exists")
out = opath+"/"


### Saving labels into dictionary
def getLabels(filename,d,label): #Preparing the labels in labelD
    with open(filename) as fid:
        for line in fid:
            d[line.rstrip()] = label
    return d
labelD = {}
for n in range(N):
 labelD = getLabels(args.f[n],labelD,args.l[n])

#ModelName = "M. leprae"
ModelName2 = args.lt[0]

###### Saving dictionary with labels
labelsO = {x:0 for x  in labelD.values()}
for el in labelD.values():
    labelsO[el] += 1
###print(labelsO,sum(labelsO.values()))
###print("******")


##### Preparing editdistances and sample indices
Target = []
def getED(filename,d,d2): # Preparing edit distances as Pest, PestTest and Pseudo...
    with open(filename) as fid:
        line = fid.readline()
#        d = {}
#        d2 = {}
        for line in fid:
            ls = line.rstrip().split()
            if ls[0] in labelD:
                d[ls[0]] = [int(x) for x in ls[1:]]
                d2[ls[0]] = labelD[ls[0]]
    return d,d2

k = 0
Pest,labelD2 = {},{}
Pseudo,labelD3 = {},{}
for el in args.lt:
 if el == args.lt[0]:
  Pest,labelD2 = getED(args.i[k],Pest,labelD2)
 elif el == args.lt[1]:
  Pseudo, labelD3 = getED(args.i[k],Pseudo,labelD3)
 k += 0

labelsO = {x:0 for x  in labelD2.values()}
for el in labelD2.values():
    labelsO[el] += 1
####print(labelsO,sum(labelsO.values()))


#####Getting targets and labels:
slist = list(labelD2.keys())
#print(labelD2.values())
kk = 0
target = []
targetreal = []
targetfalse = []
nytarget = {x:[] for x  in labelsO.keys()}
for x in slist:
    nytarget[labelD2[x]].append(kk)
    if labelD2[x] == 1:
        targetreal.append(kk)
    else:
        targetfalse.append(kk)
    kk += 1

y = numpy.array([ labelD2[x] for x in slist     ])


#This block is for ALL Labels
y = numpy.zeros(len(slist),dtype=numpy.int)
for x in range(y.shape[0]):
    if x in nytarget[ModelName2]: #ModelName = Lepra/Bovis/Tuberculosis etc
        y[x] = 1

X = []
for sample in slist:
    l = []
    for i in range(0,5): ##UNCOMMENT THIS + NEXT FOR PESTIS
        l.append( (Pest[sample][i]+1)/(1+Pseudo[sample][i]))
    rowsum = sum(Pest[sample][0:5])
    if rowsum == 0:
        rowsum = 1
    rowsum += 1
    l.extend( [(x)/rowsum for x  in Pest[sample][0:5]]   )
    
    X.append(l)
X = numpy.matrix(X) #Train data

#CrossValidation
n_samples = X.shape[0]
CV = 5
skf = StratifiedKFold(n_splits=CV,random_state=100,shuffle=True)
####print(len(slist),len(Pest.keys()),len(y))



######################
#### NAIVE BAYES #####
######################
#Naive Bayes
from sklearn.naive_bayes import GaussianNB
gnb = GaussianNB(priors=[1/2,1/2]) ###### 2 classes
score = cross_val_score(gnb, X, y, cv=skf)
#print("Accuracy: %0.2f (+/- %0.2f)" % (score.mean(), score.std() * 2))

myreport += "\n#Naive Bayes performance:\n"
myreport += "Accuracy: %0.2f (+/- %0.2f)\n" % (score.mean(), score.std() * 2)

plt.figure(figsize=(7,7))
mcc = []
aucs = []
tprs = []
mean_fpr = numpy.linspace(0, 1, 100)
kk = 0
lw = 0.2
plotx = []
ploty = []
for train, test in skf.split(X, y):
    X_train, X_test = X[train], X[test]
    y_train, y_test = y[train], y[test]
    RF = GaussianNB(priors=[1/2,1/2]) ###### 2 classes Gaussian NB
    RF.fit(X_train,y_train)
    y_pred = RF.predict(X_test)
    y_pred2 = RF.predict_proba(X_test)[:,1]
    plotx += test.tolist()
    ploty += y_pred2.tolist()
    #AUC INSTEAD OF ACCURACY SCORES!!!
    fpr, tpr, threshold = metrics.roc_curve(y_test, y_pred2,pos_label=True)
    roc_auc = metrics.auc(fpr, tpr)
    mcc.append(matthews_corrcoef(y_test,y_pred)) #MATTHEWS COEFFICIENT
    tprs.append(interp(mean_fpr, fpr, tpr))
    tprs[-1][0] = 0.0
    aucs.append(roc_auc)
    kk+=1
#print("mean_mcc",numpy.mean(mcc))
mean_tpr = numpy.mean(tprs, axis=0)
mean_tpr[-1] = 1.0
mean_auc = metrics.auc(mean_fpr, mean_tpr)
#print("mean_auc",mean_auc)
std_auc = numpy.std(aucs)
plt.plot(mean_fpr, mean_tpr, color='b',
label=r'Mean ROC',
lw=2, alpha=.7)

myreport += "mean_mcc: {0}\nmean_auc: {1}\n".format(numpy.mean(mcc),mean_auc)

std_tpr = numpy.std(tprs, axis=0)
tprs_upper = numpy.minimum(mean_tpr + std_tpr, 1)
tprs_lower = numpy.maximum(mean_tpr - std_tpr, 0)
plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                 label=r'std. dev.')
#                 label=r'$\pm$ 1 std. dev.')
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Naïve Bayes: {2}'.format(round(mean_auc,2),
                                                                               round(std_auc,2),ModelName2))
plt.legend(loc="lower right")
####plt.show()
plt.savefig("{0}plots/naivebayes_ROC.png".format(out))
plt.close()


#### NAIVE BAYES INDIVIDUAL PROBABILITIES
plt.figure()
ax = plt.subplot(111)

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width, box.height * 0.8])

plotxy = list(sorted(zip(plotx,ploty)))
k = 0

mycm = plt.cm.rainbow(  numpy.linspace(0,1,len(nytarget))   ) # COLORMAP
for xtarget,xmycm in zip(sorted(nytarget.items()),mycm):
    targetname = xtarget[0]
    mytarget = xtarget[1]
    ploty2 = [plotxy[x][1] for x  in mytarget]
    plt.scatter(range(k,len(ploty2)+k),ploty2,color=xmycm,label=targetname)
    k += len(ploty2)

plt.plot([0,len(plotxy)],[0.5,0.5],"k--")

plt.xlim([0,len(plotxy)])
plt.ylim([-0.1,1.1])
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=3,scatterpoints=1)
plt.xlabel("Data points (sorted)")
plt.ylabel("Probability")
plt.title("Naïve Bayes: {0}\nCollected results from cross-validations".format(ModelName2))
#plt.show()
plt.savefig("{0}plots/naivebayes_prob.png".format(out))
plt.close()
##### RANDOM FOREST

#RANDOM FOREST
from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(n_estimators=100,max_features=None)
score = cross_val_score(clf, X, y, cv=skf)
#print(score)
#print("Accuracy: %0.2f (+/- %0.2f)" % (score.mean(), score.std() * 2))

myreport += "\n#Random Forest performance:\n"
myreport += "RF score: {0}\n".format(score)
myreport += "Accuracy: %0.2f (+/- %0.2f)\n" % (score.mean(), score.std() * 2)

clf = clf.fit(X, y)

plt.figure(figsize=(7,7))
mcc = []
aucs = []
tprs = []
mean_fpr = numpy.linspace(0, 1, 100)
kk = 0
lw = 0.2
plotx = []
ploty = []
for train, test in skf.split(X, y):
    X_train, X_test = X[train], X[test]
    y_train, y_test = y[train], y[test]
    RF = RandomForestClassifier(n_estimators=100,max_features="auto",max_depth=3) ###### 2 classes Gaussian NB
    RF.fit(X_train,y_train)
    y_pred = RF.predict(X_test)
    y_pred2 = RF.predict_proba(X_test)[:,1]
    plotx += test.tolist()
    ploty += y_pred2.tolist()
    #AUC INSTEAD OF ACCURACY SCORES!!!
    fpr, tpr, threshold = metrics.roc_curve(y_test, y_pred2,pos_label=True)
    roc_auc = metrics.auc(fpr, tpr)
    mcc.append(matthews_corrcoef(y_test,y_pred)) #MATTHEWS COEFFICIENT
    tprs.append(interp(mean_fpr, fpr, tpr))
    tprs[-1][0] = 0.0
    aucs.append(roc_auc)
    kk+=1
#print("mean_mcc",numpy.mean(mcc))
mean_tpr = numpy.mean(tprs, axis=0)
mean_tpr[-1] = 1.0
mean_auc = metrics.auc(mean_fpr, mean_tpr)
#print("mean_auc",mean_auc)
std_auc = numpy.std(aucs)
plt.plot(mean_fpr, mean_tpr, color='b',
label=r'Mean ROC',
lw=2, alpha=.7)

myreport += "mean_mcc: {0}\nmean_auc: {1}\n".format(numpy.mean(mcc),mean_auc)


std_tpr = numpy.std(tprs, axis=0)
tprs_upper = numpy.minimum(mean_tpr + std_tpr, 1)
tprs_lower = numpy.maximum(mean_tpr - std_tpr, 0)
plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
             label=r'std. dev.')
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Random Forest: {2}'.format(round(mean_auc,2),
                                                                               round(std_auc,2),ModelName2))
plt.legend(loc="lower right")
#plt.show()
plt.savefig("{0}plots/randomforest_ROC.png".format(out))

plt.figure()
ax = plt.subplot(111)

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width, box.height * 0.8])

plotxy = list(sorted(zip(plotx,ploty)))
k = 0

mycm = plt.cm.rainbow(  numpy.linspace(0,1,len(nytarget))   ) # COLORMAP
for xtarget,xmycm in zip(sorted(nytarget.items()),mycm):
    targetname = xtarget[0]
    mytarget = xtarget[1]
    ploty2 = [plotxy[x][1] for x  in mytarget]
    plt.scatter(range(k,len(ploty2)+k),ploty2,color=xmycm,label=targetname)
    k += len(ploty2)

plt.plot([0,len(plotxy)],[0.5,0.5],"k--")

plt.xlim([0,len(plotxy)])
plt.ylim([-0.1,1.1])
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=3,scatterpoints=1)
plt.xlabel("Data points (sorted)")
plt.ylabel("Probability")
plt.title("Random Forest: {0}\nCollected results from cross-validations".format(ModelName2))
#plt.show()
plt.savefig("{0}plots/randomforest_prob.png".format(out))
plt.close()

### RANDOM FOREST FEATURE IMPORTANCES
#import matplotlib.pyplot as pplt
plt.figure()
ax = plt.subplot(111)
plt.bar(range(1,len(clf.feature_importances_)+1),clf.feature_importances_)
plt.xlabel("Feature")
plt.ylabel("Feature importance")
plt.xticks(range(1,len(clf.feature_importances_)+1))
plt.xlim([1,X.shape[1]])
plt.title("Random Forets feature importances\n{0}".format(ModelName2))
#plt.show()
plt.savefig("{0}plots/randomforest_feature_importance.png".format(out))
plt.close()


####### VARIABLE THRESHOLD FROM ROC CURVE FUNCTION
def get_threshold(fpr,tpr,threshold):
    J = []
    for FPR,TPR in zip(fpr,tpr):
        sens = TPR
        spec = 1-FPR
        J.append(sens+spec-1)
    myidx = numpy.argsort(J)[-1]
    return float(threshold[myidx])

######## ENSEMBLE VOTING CLASSIFIER
########################################################
#####                                             ######
#####  ENSEMBLE OF NAIVE BAYES AND RANDOM FOREST  ######
#####                                             ######
########################################################
from sklearn.ensemble import VotingClassifier

myreport += "\n### ENSEMBLE OF NAIVE BAYES AND RANDOM FOREST\n"

thresholdE = []
plt.figure(figsize=(7,7))
mccVC,mccNB,mccRF = [],[],[]
aucsVC,aucsNB,aucsRF = [],[],[]
tprsVC,tprsNB,tprsRF = [],[],[]
mean_fpr = numpy.linspace(0, 1, 100)
accuraciesVC = []
kk = 0
lw = 0.2
plotx = []
ploty = []
for train, test in skf.split(X, y):
    X_train, X_test = X[train], X[test]
    y_train, y_test = y[train], y[test]
    RF = RandomForestClassifier(n_estimators=100,max_features="auto",max_depth=3) ###### 2 classes Gaussian NB
    NB = GaussianNB(priors=[1/2,1/2]) ###### 2 classes Gaussian NB
    
    RF.fit(X_train,y_train)
    y_predRF = RF.predict(X_test)
    y_pred2RF = RF.predict_proba(X_test)[:,1]
    #AUC INSTEAD OF ACCURACY SCORES!!!
    fprRF, tprRF, threshold = metrics.roc_curve(y_test, y_pred2RF,pos_label=True)
    roc_aucRF = metrics.auc(fprRF, tprRF)
    mccRF.append(matthews_corrcoef(y_test,y_predRF)) #MATTHEWS COEFFICIENT
    tprsRF.append(interp(mean_fpr, fprRF, tprRF))
    tprsRF[-1][0] = 0.0
    aucsRF.append(roc_aucRF)
    
    NB.fit(X_train,y_train)
    y_predNB = NB.predict(X_test)
    y_pred2NB = NB.predict_proba(X_test)[:,1]
    #AUC INSTEAD OF ACCURACY SCORES!!!
    fprNB, tprNB, threshold = metrics.roc_curve(y_test, y_pred2NB,pos_label=True)
    roc_aucNB = metrics.auc(fprNB, tprNB)
    mccNB.append(matthews_corrcoef(y_test,y_predNB)) #MATTHEWS COEFFICIENT
    tprsNB.append(interp(mean_fpr, fprNB, tprNB))
    tprsNB[-1][0] = 0.0
    aucsNB.append(roc_aucNB)    
    
    
    VCweigths = []
    VCweigths.append(  mccRF[-1]/(mccNB[-1]+mccRF[-1])  )
    VCweigths.append(  mccNB[-1]/(mccNB[-1]+mccRF[-1])  )
    
    VC = VotingClassifier(estimators=[('RF', RF), ('NB', NB)], voting='soft', weights=VCweigths)

    VC.fit(X_train,y_train)
    y_predVC = VC.predict(X_test)
    y_pred2VC = VC.predict_proba(X_test)[:,1]
    plotx += test.tolist() ###FOR VC !!!!
    ploty += y_pred2VC.tolist() ### FOR VC !!!!
    #ACCURACY SCORES:
    accuraciesVC.append(metrics.accuracy_score(y_test,y_predVC))
    #AUC INSTEAD OF ACCURACY SCORES!!!
    fprVC, tprVC, threshold = metrics.roc_curve(y_test, y_pred2VC,pos_label=True)
    roc_aucVC = metrics.auc(fprVC, tprVC)
    mccVC.append(matthews_corrcoef(y_test,y_predVC)) #MATTHEWS COEFFICIENT
    tprsVC.append(interp(mean_fpr, fprVC, tprVC))
    tprsVC[-1][0] = 0.0
    aucsVC.append(roc_aucVC)
    mythreshold = round(get_threshold(fprVC, tprVC, threshold),2)
    thresholdE.append(  mythreshold  )
#VC
#print("VC: mean_mcc",numpy.mean(mccVC))
myreport += "mean_mcc: {0}\n".format(numpy.mean(mccVC))
mean_tpr = numpy.mean(tprsVC, axis=0)
mean_tpr[-1] = 1.0
mean_auc = metrics.auc(mean_fpr, mean_tpr)
#print("VC: mean_auc",mean_auc)
myreport += "mean_auc: {0}\n".format(mean_auc)
std_auc = numpy.std(aucsVC)
plt.plot(mean_fpr, mean_tpr, color='b',
label=r'Ensemble',
lw=2, alpha=.8)

#RF
#print("RF: mean_mcc",numpy.mean(mccRF))
mean_tpr = numpy.mean(tprsRF, axis=0)
mean_tpr[-1] = 1.0
mean_auc = metrics.auc(mean_fpr, mean_tpr)
#print("RF: mean_auc",mean_auc)
std_auc = numpy.std(aucsRF)
plt.plot(mean_fpr, mean_tpr, color='r',
label=r'Random Forest',
lw=2, alpha=.4)
myreport += "#RF: (mean_mcc = {0}\tmean_auc = {1})\n".format(numpy.mean(mccRF),mean_auc)

#NB
#print("NB: mean_mcc",numpy.mean(mccNB))
mean_tpr = numpy.mean(tprsNB, axis=0)
mean_tpr[-1] = 1.0
mean_auc = metrics.auc(mean_fpr, mean_tpr)
#print("NB: mean_auc",mean_auc)
std_auc = numpy.std(aucsNB)
plt.plot(mean_fpr, mean_tpr, color='g',
label=r'Naïve Bayes',
lw=2, alpha=.4)
myreport += "#NB: (mean_mcc = {0}\tmean_auc = {1})\n".format(numpy.mean(mccNB),mean_auc)

#Threshold determination:
thresholdE = numpy.array(thresholdE)
#print(thresholdE)
#print(numpy.mean(thresholdE))
mythreshold = numpy.mean(thresholdE)
myreport += "Thresholds: {0}\nMean_threshold_cut-off (based on ROC): {1}\n".format(thresholdE,numpy.mean(thresholdE))

###### TRAINING FULL MODEL ON ALL DATA
RF = RandomForestClassifier(n_estimators=100,max_features="auto",max_depth=3) ###### 2 classes Gaussian NB
NB = GaussianNB(priors=[1/2,1/2]) ###### 2 classes Gaussian NB

VC = VotingClassifier(estimators=[('RF', RF), ('NB', NB)], voting='soft', weights=VCweigths)
VC.fit(X,y)

#print("MEAN ACCURACY:{0}".format(numpy.mean(accuraciesVC)))
myreport += "Mean_accuracy: {0}\n\n".format(numpy.mean(accuraciesVC))

#### SAVE MODEL FOR PREDICTOR SCRIPT
from sklearn.externals import joblib
a = joblib.dump(VC, "{0}ensemble_model.pkl".format(out)) 

plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC-curve: {2}'.format(round(mean_auc,2), round(std_auc,2),ModelName2))
plt.legend(loc="lower right")
#plt.show()
plt.savefig("{0}plots/ensemble_ROC.png".format(out))
plt.close()

##### ENSEMBLE PREDICTIONS PLOT
plt.figure()
ax = plt.subplot(111)

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width, box.height * 0.8])

plotxy = list(sorted(zip(plotx,ploty)))
k = 0

mycm = plt.cm.rainbow(  numpy.linspace(0,1,len(nytarget))   ) # COLORMAP
for xtarget,xmycm in zip(sorted(nytarget.items()),mycm):
    targetname = xtarget[0]
    mytarget = xtarget[1]
    ploty2 = [plotxy[x][1] for x  in mytarget]
    plt.scatter(range(k,len(ploty2)+k),ploty2,color=xmycm,label=targetname)
    k += len(ploty2)

plt.plot([0,len(plotxy)],[0.5,0.5],"k--")

plt.xlim([0,len(plotxy)])
plt.ylim([-0.1,1.1])
#plt.legend(loc='center left')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=3,scatterpoints=1)
plt.xlabel("Data points (sorted)")
plt.ylabel("Probability")
plt.title("Ensemble: {0}\nCollected results from cross-validations".format(ModelName2))
#plt.show()
plt.savefig("{0}plots/ensemble_prob.png".format(out))
plt.close()




#FINALIZE
print("""Done! Results saved as:
- {0}plots/*png (graphs of performances)
- {0}report.txt (model performances)
- {0}ensemble_model.pkl (model for mapa_pred.py script)
""".format(out))

with open("{0}report.txt".format(out),"w") as fout:
 fout.write(myreport)

if not args.q:
 print(myreport)
