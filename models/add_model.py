import os,json,sys
spath = os.path.dirname(os.path.realpath(sys.argv[0]))

model_list = json.load(open(spath+"/model_list.json"))

model_list[sys.argv[1]] = sys.argv[2:]

json.dump(model_list, open(spath+"/model_list.json","w") ,indent=3)
