#!/usr/bin/env python
import sys,os,re,shutil,pickle,math,time
import subprocess,json,os,numpy,glob
import argparse
from matplotlib import pyplot as plt

#Setting up paths for linux/mac
spath = os.path.dirname(os.path.realpath(sys.argv[0]))
##############################
#############################
if sys.platform == "darwin": #if mac
    bpath = spath+"/binaries/darwin/"
else: #assuming linux, not windows
    bpath = spath+"/binaries/linux/"
scripts = spath + "/scripts/" #scripts path
dpath = spath + "/db/" #database path

#NEW! 2018 patch
#Replaces old code with easy database walk function
#Add database simply by making a folder with fasta-genomes
#Folders should be named after the disease.
#I.e. Folder "Salmonella" which contains salmonella genomes.
db2 = os.walk(dpath) #Get contents of db2-folder
diseases = db2.__next__()[1] #List of folders in db2

diseaselist = sorted(diseases)
dblist = "" #Sorted list of diseases
for i in range(len(diseaselist)):
 dblist += "{0}: {1}\n".format(  i+1, re.sub("_"," ",  diseaselist[i] ))


#### Getting command line arguments
parser = argparse.ArgumentParser(prog="mapaffinity_map.py",usage="python mapa_map.py -i reads1 [reads2] -o OutputFolder",description="Version 1 (2017)\nScreens (ancient) NGS reads against pathogens to determine their presence. This script maps reads to pathogens - the output of this script can be used for mapa_train.py or mapa_pred.py.",formatter_class=argparse.RawDescriptionHelpFormatter,
     epilog='''
----------------------------------------------
Available databases
----------------------------------------------
{0}

NOTE FOR BATCH-MODE USERS:
If you have multiple samples you want to check,
specify a path with -b, e.g. "-b ."
This script will then append all editdistances to
files in this path, so you don't have to manually
concatenate all files.

See https://bitbucket.org/kosaidtu/mapaffinity for full documentation.'''.format(dblist))
parser.add_argument("-i", metavar="FILE",help="Input single/paired fastq-files. (Required)",nargs="+",required=True)
parser.add_argument("-o", metavar="NAME",help="Folder for output. (Required)",nargs=1,required=True)
parser.add_argument("-t", metavar="INT",help="Number of threads. Default=2",type=int,default=2)
parser.add_argument("-d", metavar="INT",help="Space-separated list of databases to search e.g. '-d 1 3'. Default is to screen all databases.",type=int,nargs="+")
parser.add_argument("-b",metavar="PATH",help="BATCH-mode: specify output-folder here, where all editdistances will be appended to a single file per organism.",nargs=1)
parser.add_argument("--path",help="Use programs in $PATH instead of included pre-compiled binaries",action="store_true")
parser.add_argument("--delete",help="Delete BAM-files and other large temporary files.",action="store_true")

args = parser.parse_args()

print(args)

if args.path: #Use local software instead
    bpath = ""

#Crawl over each folder and parse 2 or more fasta-files
names = {} # Fasta-file --> fasta-ID
namesACC = {} # fasta-ID --> fasta-file
overview = {} # Diseases --> list of fasta files
path2rpath = {} # Fasta-file --> path to fasta-file
bwa_suffices = set(["sa","amb","ann","pac","bwt"])
fasta_suffices = set(["fna","fa","fasta","faa","genome","fastafile",""])
for el in db2: #db2 is the iterator from above. It's still active
    disease = el[0].split("/")[-1]
    fastafiles = [ _ for _ in el[-1] if _.split(".")[-1] not in bwa_suffices]
    fastafiles = [ _ for _ in fastafiles if _.split(".")[-1] in fasta_suffices   ]

    overview[disease] = fastafiles    
    path2rpath.update( dict(zip(fastafiles,  ["{0}/{1}".format(disease,_) for _ in fastafiles] )) )

    if len(el[-1]) == 0:
        print("Skipped {0}. Empty folder.")
        continue #FUTURE: Add fasta-validation here as well.
    for i in fastafiles:
        with open(el[0]+"/"+i) as fid:
            line = fid.readline()[1:]
            ls = line.split()
            names[i.split("/")[-1]] = " ".join(ls[1:3])
            namesACC[" ".join(ls[1:3])] = ls[0]
#json.dump([names,namesACC],open("{0}/db.json".format(dpath),"w"),indent = 3) #Removed May 2018, to avoid write-permissions for db/ folder.

#### Determine which databases to run against
runlist = []
if not args.d: #Screen 
    runlist = diseaselist
else:
    try: #Checking if user specified valid input
        runlist = [diseaselist[x-1] for x  in args.d]
    except:
        sys.exit("Something wrong with -d input. Examples: '-d 2' or '-d 1 2 3'. Type -h for a list.")



        
#### Preparing output-folder
t = args.t
opath = args.o[0]
if not os.path.exists(opath):
    os.mkdir(opath)
    os.mkdir(opath+"/tmp")
    os.mkdir(opath+"/ref")
#    os.mkdir(opath+"/plots")
    os.mkdir(opath+"/log")    
else:
    print("Warning! Output folder already exists! Will skip existing mappings")
#    sys.exit("Output folder already exists")
out = opath+"/"

#### Preparing batch-mode output folder
if args.b:
    if not os.path.exists(args.b[0]):
        print("Creating folder: {0}".format(args.b[0]))
        os.mkdir(args.b[0])

#### Checking if input files are ok
# Add fastq-validator later...
for el in args.i:
    if not os.path.exists(el):
        sys.exit("Input fastq-file does not exist!: {0}".format(el))

def pprint(s):
    print(s)
 

#### Running mapping

report = ""
for genus in runlist: #For every genus family
    
    #For plotting purposes
    plotlist = list(set(overview[genus])) #pathogenlist
    
    #print("\nMapping against: {0}".format(plotlist))
    print("\nMapping against: {0}".format( [names[_] for _ in plotlist]     ))
    nsamples = len(plotlist) #Number of pathogens in plot
    ed = 5 #Edit distances to calculate
    pn = 0 #pathogennumber
    x2 = range(0,ed)
    editlist = []
    
    badindex = set()
    pnn = 0
    for path in plotlist: #For every pathogen in the genus
#        print("Mapping now against: {0} ({1})".format(names[path],path))
        #### Prepare sample variables
        sample = "{0}".format(dpath+path2rpath[path])
        sampleName = out+"ref/"+re.sub(" ","_",names[path])
        #sampleGroup = path2dis[path]
        if os.path.exists(sampleName+".json"):
            print("Skipping. Already mapped against: {0} ({1})".format(names[path],path))
            badindex.add(pnn)
            pnn += 1
            continue
        else:
            print("Mapping now against: {0} ({1})".format(names[path],path))

	#### First check if there is an index, otherwise create one
        for bwai in ["sa","pac","bwt","ann","amb"]:
            if not os.path.exists("{0}.{1}".format(sample,bwai)):
                print("Now indexing {0} ({1})".format(names[path],path))
                p = subprocess.check_output('{0}bwa index {1}'.format(bpath,sample), stderr=open(out+"log/programs.log","w"),shell=True)

        if len(args.i) == 1 and not os.path.exists(sampleName+".json"): #### ADD IF FILE NOT EXISTS JSON (CONTINUE FUNCTION)
            r1 = args.i[0]
            temp = out+"tmp/"+re.sub(" ","_",names[path])

#Map
            p = subprocess.check_output('{0}bwa aln -l 1024 -t {4} {3} {2} | {0}bwa samse {3} - {2} | {0}samtools view -F 4 -h -q 30 - | python {1}filter_readlength.RE.py | samtools sort - > {6}.bam '.format(bpath,scripts,r1,sample,t,sampleName,temp), shell=True,stderr=open(out+"log/programs.log","w"))
#Deduplicated PCR
            p = subprocess.check_output('java -Xmx4g -jar {0}picard.jar MarkDuplicates I={2}.bam O={2}.rmdup.bam M={2}.rmdup.metrics REMOVE_DUPLICATES=true'.format(bpath,scripts,temp,sample,t,), shell=True,stderr=open(out+"log/programs.log","w"))

#Calculate editdistances
            p = subprocess.check_output('{0}samtools view -S {2}.rmdup.bam | python {1}get_editdist.RE.py {5}'.format(bpath,scripts,temp,sample,t,sampleName), shell=True,stderr=open(out+"log/programs.log","w"))


#            p = subprocess.check_output('{0}bwa aln -l 1024 -t {4} {3} {2} | {0}bwa samse {3} - {2} | {0}samtools view -S - | python {1}get_editdist.RE.py {5}'.format(bpath,scripts,r1,sample,t,sampleName), shell=True,stderr=open(out+"log/programs.log","w"))

            a = p.decode("ASCII").rstrip().split(",")
            if a[0]:
                a = [int(x) for x in a]
                editlist.append(dict(zip(a[0::2],a[1::2])))
            else:
                editlist.append({0:0})
        else:
            r1 = args.i[0]
            r2 = args.i[1]
            tempsai = out+"tmp/"+r1.split("/")[-1]+".TMP.sai"
            tempsai = out+"tmp/"+re.sub(" ","_",names[path])+".forwardreads.sai"
            temp = out+"tmp/"+re.sub(" ","_",names[path])
            #0 bpath,
            #1 spath
            #2 r1
            #3 r2 
            #4 contigs.fa (sample)
            #5 temp
            #6 threads
            #7 sampleName

            

            p1 = subprocess.check_output("{0}bwa aln -l 1024 -t {3} {2} {1} > {4}".format(bpath,r1,sample,t,tempsai),shell=True,stderr=open(out+"log/programs.log","w"))

            p = subprocess.check_output('{0}bwa aln -l 1024 -t {6} {4} {3} | {0}bwa sampe {4} {5} - {2} {3} | {0}samtools view -F 4 -h -q 30 - | python {1}filter_readlength.RE.py | samtools sort - > {8}.bam'.format(bpath,scripts,r1,r2,sample,tempsai,t,sampleName,temp), shell=True,stderr=open(out+"log/programs.log","w"))

#Deduplicated PCR
            p = subprocess.check_output('java -Xmx4g -jar {0}picard.jar MarkDuplicates I={2}.bam O={2}.rmdup.bam M={2}.rmdup.metrics REMOVE_DUPLICATES=true'.format(bpath,scripts,temp,sample,t,), shell=True,stderr=open(out+"log/programs.log","w"))

#Calculate editdistances
            p = subprocess.check_output('{0}samtools view -S {2}.rmdup.bam | python {1}get_editdist.RE.py {5}'.format(bpath,scripts,temp,sample,t,sampleName), shell=True,stderr=open(out+"log/programs.log","w"))




#            p = subprocess.check_output('{0}bwa aln -l 1024 -t {6} {4} {3} | {0}bwa sampe {4} {5} - {2} {3} | {0}samtools view -S - | python {1}get_editdist.RE.py {7}'.format(bpath,scripts,r1,r2,sample,temp,t,sampleName), shell=True,stderr=open(out+"log/programs.log","w"))
            a = p.decode("ASCII").rstrip().split(",")
            if a[0]:
                a = [int(x) for x in a]
                editlist.append(dict(zip(a[0::2],a[1::2])))
            else:
                editlist.append({0:0})
        pn += 1
    
    pnnnn = 0 #subtract from editlist...
    for i in range(len(plotlist)):
        fileout = re.sub(" ","_",names[plotlist[i]])
        if i in badindex:
            pnnnn += 1
            continue
        with open(out+"editdistances."+fileout,"a") as fout:
            fout.write( args.i[0].split("/")[-1])
            for ii in sorted(editlist[i-pnnnn].keys()):
                fout.write("\t{0}".format(editlist[i-pnnnn][ii]))
            fout.write("\n")

        #Batch-mode
        if args.b:
            if args.b[0] == args.o[0]:
                print("Warning! -o and -b identical. Editdistances will contain duplicates.")
            with open(args.b[0]+"/editdistances."+fileout,"a") as fout:
                fout.write( args.i[0].split("/")[-1])
                for ii in sorted(editlist[i-pnnnn].keys()):
                    fout.write("\t{0}".format(editlist[i-pnnnn][ii]))
                fout.write("\n")


print("DONE! Files saved to: {0}".format(out))
if args.delete:
    print("\nDeleting bam files...")
    for lfile in glob.glob(out+"tmp/*bam"):
        print("removing: {0}".format(lfile))
        os.remove(lfile)
    for lfile in glob.glob(out+"tmp/*sai"):
        print("removing: {0}".format(lfile))
        os.remove(lfile)


print("------------------")
if args.b:
    print("To predict, write:")
    print("python mapa_pred.py -i {0}/editdistances.*".format(args.b[0]))
else:
    print("To predict, write:")
    print("python mapa_pred.py -i {0}/editdistances.*".format(args.o[0]))


