#About:
Detects/screens ngs fastq samples for Y.pestis, M. tuberculosis and M. leprae.
Can also be trained to detect any other organisms using a Random Forest/Naive Bayes
ensemble model.

#License:
Free.

#Dependencies:
- Python3
    - Python3-package: numpy 1.10.2
    - Python3-package: scikit-learn 0.19.1
    - Python3-package: matplotlib 1.5.1
- Java (tested with v1.8.\*)

Other versions of numpy and matplotlib should work. Using other versions of scikit-learn than 0.19.1 can give a warning during prediction, but may still work.

MapAffinity only works for Linux (and Mac) systems.

#How to install:
    git clone bitbucket.com/kosaidtu/mapaffinity

Make sure that the folder "db" and the folders within are write-able.

#Overview of program and usage:
MapAffinity consists of these three main scripts:

- **mapa_map.py:** Maps fastq-files to reference strains
- **mapa_pred.py:** Predicts probabilities of organism-presence from output of mapa_map.py
- **mapa_train.py:** Trains an ensemble of Random Forest and Naive Bayes based on a set of samples mapped with mapa_map.py

The first step is to map fastq-files with mapa_map.py and the second step is to predict the presence of an organism with mapa_pred.py. The script mapa_train.py is only used for training new models for new organisms.

For most users, you will likely only need to use mapa_map.py and mapa_pred.py. 
**Common usage**
If you have a sample, "mysample", the general usage is this:
```
python mapa_map.py -i mysample.fq (mysample_reverse.fq) -o mysample
python mapa_pred.py -i mysample/editdistances*
```

The next section shows usage scenarios in more detail.

#Tutorial:
Here are two typical usage scenarios. For briefity, the examples assume you run everything inside the MapAffinity folder.
## Example 1: I have a sample, and want to see which organisms are present
For each sample (here, **sampleN**), run the following:
```
    python mapa_map.py -i sample1.fq -o sample1 -b ./merged
    python mapa_map.py -i sample2.fq -o sample2 -b ./merged
    python mapa_map.py -i sample3.fq -o sample3 -b ./merged
    (...)
```
I strongly recommend using the -b argument if you have more than 1 sample, otherwise you would have to manually concatenate the results from each output-folder. The path you specify with -b has to be the same for all samples, but you can change the output folder -o for each individual sample. If -b and -o are identical, the outputted results will have duplicate entries.

Now run the predictor like this:

    python mapa_pred.py -i ./merged/* -o myreport.txt

The report will be printed to STDOUT (your screen) unless you specify a report name with the -o option.

Note that the program supports both single and paired end reads.
It also supports gzipped fastq-files.
If you are only interested in specific organisms, you can save time by not mapping to all databases. Type the following:
    python mapa_map.py -h
This will print the help-message with a And then see which database you want to use. For example, if you only want to screen
for Y. pestis, just add "-d 1" in the above command:
    python mapa_map.py -i sampleN.fq -o sampleN -b ./merged -d 1

## Example 2: I want to screen for an organism which is not in the database
Say you have an unknown ancient sample, sampleX.fq and want to screen for an organism which is not in the included database. This requires that you train mapaffinity to the organism you want to search for.
Luckily, Mapaffinity is optimized for single computers, so you do not need a lot of memory or CPU power.
However, you still need to *train* your training data set.

You need to download samples that you know are positives (i.e. contain the organism you search for) and samples you know are negatives (i.e. samples that do NOT contain the organism you have, preferably organisms that are closely related to your organism of interest). As an example, let's say you want to screen for Salmonella paratyphi C.

First you download fastq-samples from NCBI SRA or ENA that have this bacteria.

Then you download fastq-samples that are close to this bacteria but ARE NOT Salmonella paratyphi C. Preferably closely related organisms.

Now create two empty text files, one with the organisms that are positive, and one for the organisms that are negative. Use any plain-text-editor.
Call them salmonella_C.txt and salmonella_regular.txt.

Next, you need to download the reference genomes of Salmonella paratyphi C and a closely related organism. This could be a regular modern salmonella bacteria, but you can play around with different genomes. For example, for the Y. pestis classifier, we used the organism Y. pseudotuberculosis as a closely related organism.
Get these two genomes from e.g. the NCBI nucleotide database, and make a folder in the "db" directory:

    mkdir db/salmonella_paratyphi_C
    chmod a+w db/salmonella_paratyphi_C

Put the fasta-files for the two genomes into this folder.
Now type:

    python mapa_map.py -h

Note down the database number for "salmonella_paratyphi_C".
Let's say it is database number 4. Now, for each of the downloaded fastq-files, you
type:

    python mapa_map.py -i sampleN.fq.gz -o sampleN -b ./merged -d 4

Lets say your two reference genomes are called *S_paratyphi_C* and *S_regular*. Create two files that each contains the samples for a particular organism. For example:
```
>cat S_paratyphi_C.txt

sample1.fq
sample2.fq
sample4.fq
sample7.fq
```

```
>cat S_regular.txt

sample3.fq
sample5.fq
sample6.fq
```

Now we can train the model by typing:

    python mapa_train.py -i ./merged/editdistances.S_paratyphi_C.fna ./merged/editdistances.S_regular.fna -f S_paratyphi_C.txt S_regular.txt -l Salmonella_paratyphi_C Salmonella_regular -lt Salmonella_paratyphi_C Salmonella_regular -o Salmonella_paratyphi_C_model

You can keep the arguments to -l and -lt the same. However, if your training set consists of multiple organisms, you can specify those with labels to the "-l" argument. Just make sure you have a corresponding number of files for the -f argument. This is useful if you want to determine which organism to use as the closely related, as the plots will show how each organism group perform. However, this is not strictly necessary, and it is ok if you group anything that isn't your target organism as one group.

After running, you will see a lot of statistics about the performance.
These are also saved in a text file in the folder **"Salmonella_paratyphi_C_model"**.
In here you will also find various plots about the performances, such as ROC curves and Random Forest feature importance plots.
In the report you will also find a recommended threshold based on Youden's J statistic (https://en.wikipedia.org/wiki/Youden%27s_J_statistic). You can use this threshold to evaluate the predicted probabilities, instead of using a flat 0.5 cut-off.

Your model will saved as: **Salmonella_paratyphi_C_model/ensemble_model.pkl**

Now we can map your unknown sample:

    python mapa_map.py -i sampleX.fq -d 4 -o sampleX

And the prediction can be found by typing:

    python mapa_pred.py -i sampleX/editdistances.S_paratyphi_C sampleX/editdistances.S_regular -m Salmonella_paratyphi_C_model/ensemble_model.pkl

### Example 2.1: I want to permanently add this model to the script.
Copy the model to the **"models"** folder:

    cp Salmonella_paratyphi_C_model/ensemble_model.pkl models/ensemble_model.pkl

If you have permission problems, you may need to grant write-permission to the models folder (e.g. "chmod go+w models").

Now, run the following script:

    python models/add_model.py models/ensemble_model.pkl S_paratyphi_C.fna S_regular.fna

Just make sure that you use the exact same labels that the mapa_map.py script outputs as suffix in the editdistance-files.
Now, your model is added to the program permanently. This means that you can run the predictor like this and the program will map to the standard organisms as well as your organism:

    python mapa_map.py -i sampleX.fq -o sampleX
    python mapa_pred.py -i sampleX/editdistances.*

# Known issues:
To avoid re-indexing the genomes, the program requires that the folder "db" in the program folder is writable by the user. I know this may be an issue for some systems so if you are a system administrator, go to the MapAffinity folder, and (in Bash) write:
```
    for i in db/*/; do cd $i; for ii in *fna; do bwa index $ii; done; cd ../../; done
```
This prevents indices to be created on the fly. However, for adding new databases, you still need access to both the folder db/ and models/. 

# References:
For the math and understanding what is happening "under-the-hood", please refer to our article:

- *Al-Nakeeb, K, Klincke, F. and Rasmussen, S.: "MapAffinity",tbd (2018)*
