#!/usr/bin/env python
import argparse
import os,sys,re,numpy,glob
import json
from sklearn.externals import joblib

parser = argparse.ArgumentParser(prog="mapaffinity_pred",usage="python mapa_pred.py -i editdistances.A editdistances.B -m modelA.pkl -o report.txt",description="Version 1 (2018)\nScreens (ancient) NGS reads against pathogens to determine their presence. This script predicts the probabilities from a trained model.",formatter_class=argparse.RawDescriptionHelpFormatter,
     epilog='''
----------------------------------------------
Tutorial
----------------------------------------------
For two organisms, A and B, run mapa_map.py
to get the following files:
- editdistances.A
- editdistances.B

Run this program like this:
python mapa_pred.py
-i editdistances.A editdistances.B
(-m models/A.pkl)
(-o output_report.txt)

Note that the two input files from -i must be
in order, i.e. positive pathogen first (A) and 
closely related organism second (B) if you
use your own model with the -m argument.

See https://bitbucket.org/kosaidtu/mapaffinity for full documentation.''')
parser.add_argument("-i", metavar="FILE",help="Input SPACE-sep. list of editdistances files. (Required)", nargs="+",required=True)
parser.add_argument("-m", metavar="FILE",help="Input model. Keep blank if predicting against built-in databases.", nargs=1)
parser.add_argument("-o", metavar="NAME",help="Output report file name",nargs=1,required=False)
parser.add_argument("-t", metavar="INT",help="Number of threads. Default=2",type=int,default=2)
#parser.add_argument("--path",help="Use programs in $PATH instead of included pre-compiled binaries",action="store_true")

args = parser.parse_args()
#args = parser.parse_args('-i naive_bayes/salmonella/fasta/{0}_1.fastq naive_bayes/salmonella/fasta/{0}_2.fastq -o ./naive_bayes/salmonella/{0} -d 3'.format(salm).split()) #test

print(args)

#Setting up paths
spath = os.path.realpath(sys.argv[0])
mpath = os.path.dirname(spath)+"/models/"
#Loading model
modellist = []
if not args.m: #Standard models
    models = json.load(open(mpath+"/model_list.json"))
    input_extensions = [ _.split(".")[-1] for _ in args.i ]
    for model in models:
        model_intersection = set.intersection(set(models[model]),set(input_extensions))
        if len( model_intersection  ) == 2:
            tempL = [     "{0}{1}".format(mpath,model)    ]
            tempL.append(   [input_extensions.index(_) for _ in models[model] ]   )
            modellist.append( tempL  )
elif os.path.exists(args.m[0]):
    if len(args.i) != 2:
        sys.exit("You need two input files if you specify -m.")
#    modellist = [  joblib.load(open("{0}".format(args.m[0]),"rb")), args.i ]
    modellist.append( [ args.m[0]   ,[0,1]])
#####    modellist.append( [ args.m[0]   ,args.i])

#    VC = joblib.load(open("{0}".format(args.m[0]),"rb"))
else:
    sys.exit("Could not find model: {0}".format(args.m))





#GET EDIT DISTANCES
def getED(filename): # Preparing edit distances as Pest, PestTest and Pseudo...
    with open(filename) as fid:
        #line = fid.readline()
        d = {}
        d2 = {}
        for line in fid:
            ls = line.rstrip().split()
            #if ls[0] in labelD:
            d[ls[0]] = [int(x) for x in ls[1:]]
            while len(d[ls[0]]) < 5:
                d[ls[0]].append(0)
            #d2[ls[0]] = labelD[ls[0]]
    return d,d2


#Function to calculate features/attributes
def getFeatures(RVlist,PestRV,PseudoRV):
    Xtemp = []
    Lrowsum = []
    for sample in RVlist:
        l = []
        for i in range(0,5):
            l.append( (1+PestRV[sample][i])/(1+PseudoRV[sample][i]))
        rowsum = sum(PestRV[sample][0:5])
        Lrowsum.append(rowsum)
        if rowsum == 0:
            rowsum = 1
        rowsum += 1
        l.extend( [(x)/rowsum for x in PestRV[sample][0:5]]   )
    
        Xtemp.append(l)

    
    Xtemp = numpy.matrix(Xtemp) #Train data
    return Xtemp,Lrowsum


myreport = ""
for el in modellist:
#    PestRV,labelD2RV = getED(args.i[0]) #FP_lepra
#    PseudoRV,labelD2RV = getED(args.i[1]) #FP_bovis

    PestRV,labelD2RV = getED(args.i[el[1][0]]) #FP_lepra
    PseudoRV,labelD2RV = getED(args.i[el[1][1]]) #FP_bovis
    RVlist = list(PestRV.keys())
    XRV,Lrowsum = getFeatures(RVlist,PestRV,PseudoRV)

    VC = joblib.load(open("{0}".format(el[0]),"rb"))


    #Calculating the probabilities (predictions)
    VCpred = VC.predict_proba(XRV)
    print("#### {0}\n#Sample ID\t#Prob.\t#_mapped_reads_to_target".format(el[0].split("/")[-1]    ))
    myreport += "#### {0}\n#Sample ID\t#Prob.\t#_mapped_reads_to_target\n".format(el[0].split("/")[-1])
    for i in range(VCpred.shape[0]):
    #    if VCpred[i,1] > 0.90:
#        print(RVlist[i],VCpred[i,1])
        predval = str(round(VCpred[i,1],5)) #Probability
        #Hardcoding "-" when no reads are mapped
        if Lrowsum[i] == 0:
            predval = "-"
        print("{0}\t{1}\t{2}".format(RVlist[i],predval,Lrowsum[i]))
        myreport += "{0}\t{1}\t{2}\n".format(RVlist[i],predval,Lrowsum[i])
    print("")
    myreport+= "\n"

if args.o:
 with open(args.o[0],"w") as fout:
  sup = fout.write(myreport)

